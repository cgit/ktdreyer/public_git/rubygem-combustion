%global gem_name combustion

Name: rubygem-%{gem_name}
Version: 0.5.1
Release: 1%{?dist}
Summary: Elegant Rails Engine Testing
Group: Development/Languages
License: MIT
URL: https://github.com/pat/combustion
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
%if 0%{?fc19} || 0%{?fc20} || 0%{?el7}
Requires: ruby(release)
Requires: ruby(rubygems)
Requires: rubygem(activesupport) >= 3.0.0
Requires: rubygem(railties) >= 3.0.0
Requires: rubygem(thor) >= 0.14.6
%endif
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildArch: noarch
%if 0%{?fc19} || 0%{?fc20} || 0%{?el7}
Provides: rubygem(%{gem_name}) = %{version}
%endif

%description
Test your Rails Engines without needing a full Rails app


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

for f in .gitignore Gemfile Rakefile; do
  rm $f
  sed -i "s|\"$f\",||g" %{gem_name}.gemspec
done

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

%gem_install

# remove unnecessary gemspec
rm .%{gem_instdir}/%{gem_name}.gemspec

%install
mkdir -p %{buildroot}%{gem_dir}
cp -pa .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/


mkdir -p %{buildroot}%{_bindir}
cp -pa .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{gem_instdir}/bin -type f | xargs chmod a+x

%files
%dir %{gem_instdir}
%doc %{gem_instdir}/LICENCE
%doc %{gem_instdir}/README.md
%{_bindir}/combust
%{gem_instdir}/bin
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}
%{gem_instdir}/templates

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/HISTORY

%changelog
* Wed May 07 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.5.1-1
- Initial package
